import java.util

object Generator extends App{

  val months = List[String]("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12")

  val monthName = List[String]("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")

  val map = new util.HashMap[String, String]

  val numberOfDaysMap = new util.HashMap[String, Integer]

  main()

  def main(): Unit = {

    loadMaps()

    //generateMonths(18, 216)

    generateDays(216, 6575)

  }

  def loadMaps(): Unit = {

    map.put("01", "January")
    map.put("02", "Feburary")
    map.put("03", "March")
    map.put("04", "April")
    map.put("05", "May")
    map.put("06", "June")
    map.put("07", "July")
    map.put("08", "August")
    map.put("09", "September")
    map.put("10", "October")
    map.put("11", "November")
    map.put("12", "December")

    numberOfDaysMap.put("January", 31)
    numberOfDaysMap.put("February", 28)
    numberOfDaysMap.put("March", 31)
    numberOfDaysMap.put("April", 30)
    numberOfDaysMap.put("May", 31)
    numberOfDaysMap.put("June", 30)
    numberOfDaysMap.put("July", 31)
    numberOfDaysMap.put("August", 31)
    numberOfDaysMap.put("September", 30)
    numberOfDaysMap.put("October", 31)
    numberOfDaysMap.put("November", 30)
    numberOfDaysMap.put("December", 31)

  }

  def generateMonths(idYear: Integer, lastIdMonth: Integer): Unit = {


    months.foreach(m => {

      val month = map.get(m)

      val idMonth = lastIdMonth + Integer.parseInt(m)

      println(s"INSERT INTO CEM_MONTH (ID_MONTH, NAME, ID_YEAR, MONTH_NAME) VALUES (${idMonth}, '${m}', ${idYear}, '${month}');")

    })

  }

  def generateDays(lastIdMonth: Integer, lastIdDay: Integer): Unit = {

    var idDay = lastIdDay

    var idMonth = lastIdMonth

    monthName.foreach(m => {

      idMonth += 1

      val daysInMonth = numberOfDaysMap.get(m)

      for (i <- 1 until daysInMonth+1) {

        idDay += 1

        println(s"INSERT INTO CEM_DAY (ID_MONTH, ID_DAY) VALUES (${idMonth}, ${idDay});")

      }

    })


  }

}
